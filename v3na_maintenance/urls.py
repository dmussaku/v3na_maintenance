from django.conf.urls import patterns, include, url

from django.contrib.auth import views as contrib_auth_views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()


urlpatterns = patterns('',
	url(r'^server/', include('v3na.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'} ),
	

    # Examples:
    # url(r'^$', 'v3na_maintenance.views.home', name='home'),
    # url(r'^v3na_maintenance/', include('v3na_maintenance.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
