# Django settings for v3na_maintenance project.

import os
import sys
import imp




def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('apps'))
'''
import mongoengine as me
me.connect("v3nadb")
'''
#SENDSMS_BACKEND = 'sendsms.backends.console.SmsBackend'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

#AUTH_USER_MODEL = 'mongo_auth.MongoUser'


#MONGOENGINE_USER_DOCUMENT='v3na.models.User'

APP_DIR = os.path.dirname( globals()['__file__'] )

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'v3nadb',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}


# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Almaty'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(APP_DIR, '../staticfiles/')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n#_*u&l8r^a*i^!#f^+02ot1=p&qe&o8goc0spp+)7r(5u#u8*'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
   
    'django.contrib.messages.middleware.MessageMiddleware',

    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'v3na_maintenance.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'v3na_maintenance.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join( APP_DIR, 'templates' ),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# The backend used to store task results - because we're going to be 
# using RabbitMQ as a broker, this sends results back as AMQP messages



INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'v3na',
    'djcelery','instagram_api',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin','django_twilio','gunicorn', 
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

TWILIO_ACCOUNT_SID = 'AC8ff6dd2a802b5647a66e609dcf68475b'
TWILIO_AUTH_TOKEN = '59faa85185adfbc17ff014049ecc31b4'
TWILIO_PHONE_NUMBER='+14252797485'
TWILIO_DEFAULT_CALLERID = 'NNNNNNNNNN'


TWILIO_ACCOUNT_SID2 = 'ACb970fba7a1048377845255b00a0f6432'
TWILIO_AUTH_TOKEN2 = 'e46474cec3453bcfa9b923652eeea494'
TWILIO_PHONE_NUMBER2='+12284714034'
TWILIO_DEFAULT_CALLERID2 = 'NNNNNNNNNN'

TWILIO_ACCOUNT_SID3 = 'AC7379b0a521c9bc519f5db87c9b636951'
TWILIO_AUTH_TOKEN3 = '441cc7a902a2e671744492cca75fa43a'
TWILIO_PHONE_NUMBER3='+12247574299'
TWILIO_DEFAULT_CALLERID3 = 'NNNNNNNNNN'

import djcelery
import celery
djcelery.setup_loader()
from celery.schedules import crontab
from datetime import timedelta

BROKER_URL='amqp://guest:guest@localhost:5672//'

CELERY_RESULT_BACKEND = "amqp"
CELERY_TIMEZONE = 'Asia/Almaty'
#CELERY_IMPORTS = ("v3na.tasks", )
#CELERY_ALWAYS_EAGER = True
#CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
'''
CELERYBEAT_SCHEDULE = {
    'server-check-every-5-minutes-insettings': {
        'task': 'v3na.tasks.server_task',
        'schedule': timedelta(minutes=5)
    },
}
'''

# The default Django db scheduler


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TEMPLATE_CONTEXT_PROCESSORS = (
    
    'django.contrib.auth.context_processors.auth',
    

)

LOGIN_REDIRECT_URL='/server/' 

#import mongo_auth.contrib.models
#USER_CLASS=mongo_auth.contrib.models.User

