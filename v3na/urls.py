from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
# import django_twilio
from v3na.views import AddUrlView, UrlTemplateView, try_url, UrlDeleteView, UrlUpdateView, delete_object
from django.contrib.auth import views as contrib_auth_views

urlpatterns = patterns('',
	url(r'^addurl/$', login_required(AddUrlView.as_view()), name='addurl'),
    url(r'^tryurl/(?P<id>\d+)/$', try_url, name='try_url'),
    url(r'^$', login_required(UrlTemplateView.as_view()), name='urls'),
    url(r'^update/(?P<pk>\d+)/$', login_required(UrlUpdateView.as_view()), name='update_url'),
    url(r'^delete/(?P<pk>\d+)/$', login_required(UrlDeleteView.as_view()), name='delete_url'),

    


    #http://www.villagegeek.com/downloads/webwavs/airplane.wav
    #url(r'^update/(?P<usr>\w+)/$', Task.views.update, name='update'),

    # Examples:
    # url(r'^$', 'testproject.views.home', name='home'),
    # url(r'^testproject/', include('testproject.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)