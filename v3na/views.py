# Create your views here.
from django.shortcuts import render_to_response, render, redirect
from models import V3na_Server
from django.http import HttpResponse, HttpResponseRedirect
import urllib2
from django.views.generic import TemplateView, CreateView, DeleteView, UpdateView

from django import forms
from tasks import try_server, send_text
from forms import V3naForm


'''
def delete_object(request, id):
	a=V3na_Server.objects.filter(id=id)
	print a
	a.delete()
	return HttpResponse('deleted')
'''
class AddUrlView(CreateView):
	model=V3na_Server
	form=V3naForm
	template_name='add_url.html'
	success_url = '/server/'
	fields=['url_link']

class UrlDeleteView(DeleteView):
	template_name='delete_url.html'
	model=V3na_Server
	success_message='deleted'
	success_url='/server/'
	def get_object(self, queryset=None):
		pk=self.kwargs['pk']
		return V3na_Server.objects.get(pk=pk)

class UrlUpdateView(UpdateView):
	template_name='update_url.html'
	form=V3naForm
	model=V3na_Server
	success_url = '/server/'
	fields=['url_link']
	def get_queryset(self):
		pk=self.kwargs['pk']
		return V3na_Server.objects.filter(pk=pk)

class UrlTemplateView(TemplateView):
	template_name='url.html'
	def get_context_data(self, **kwargs):
		context=super(UrlTemplateView, self).get_context_data(**kwargs)
		context["urls"]=V3na_Server.objects.all()
		return context

def try_url(request, id):
	url=V3na_Server.objects.get(id=id).url_link
	return HttpResponse(try_server(url))

def delete_object(request, id):
	a=V3na_Server.objects.get(id=id)
	a.delete()
	return HttpResponse('deleted')



