from models import V3na_Server
import urllib2
import celery
from celery.task import task, periodic_task
from celery.task.schedules import crontab
from datetime import timedelta
from twilio.rest import TwilioRestClient
from v3na_maintenance import settings
from instagram.client import InstagramAPI
from instagram_api.tasks import access_token 


def try_server(url):
	try:
		urllib2.urlopen(url)
	except urllib2.HTTPError, err:
		if err.code == 404 or err.code == 500:
			return False
	except urllib2.URLError:
		return False
	return True

def send_text(text, number):
	account_sid = settings.TWILIO_ACCOUNT_SID
	auth_token  = settings.TWILIO_AUTH_TOKEN
	client = TwilioRestClient(account_sid, auth_token)
	message = client.messages.create(body=text, to=number, from_=settings.TWILIO_PHONE_NUMBER)

def send_text2(text, number):
	account_sid = settings.TWILIO_ACCOUNT_SID2
	auth_token  = settings.TWILIO_AUTH_TOKEN2
	client = TwilioRestClient(account_sid, auth_token)
	message = client.messages.create(body=text, to=number, from_=settings.TWILIO_PHONE_NUMBER2)	

def send_text3(text, number):
	account_sid = settings.TWILIO_ACCOUNT_SID3
	auth_token  = settings.TWILIO_AUTH_TOKEN3
	client = TwilioRestClient(account_sid, auth_token)
	message = client.messages.create(body=text, to=number, from_=settings.TWILIO_PHONE_NUMBER3)	


#@celery.decorators.periodic_task(run_every=timedelta(minutes=5))
@celery.decorators.periodic_task(run_every=crontab(minute='*/5', hour='7-0'))
def server_task():
	from v3na.models import V3na_Server
	from v3na.tasks import try_server
	from v3na.tasks import send_text

	for obj in V3na_Server.objects.all():
		out=try_server(obj.url_link)
		if out==False and obj.flag==1:
			obj.flag=0
			obj.save()
			send_text(str(obj.url_link)+' failed to start','+77773713880')
			send_text2(str(obj.url_link)+' failed to start','+77770077705')
			send_text3(str(obj.url_link)+' failed to start','+77772962124')
			return obj.url_link

		if out==True and obj.flag==0:
			obj.flag=1
			obj.save()
			send_text(str(obj.url_link)+' is back on track','+77773713880')
			send_text2(str(obj.url_link)+' is back on track','+77770077705')
			send_text3(str(obj.url_link)+' is back on track','+77772962124')

@celery.decorators.periodic_task(run_every=crontab(hour='*/24'))
def feed_like():
	#access_token = "46695212.7e028dd.e4ce491f1aff44cf8b4315af0293d9f8"
	api = InstagramAPI(access_token=access_token)
	media_list=api.user_media_feed(count=50)
	media_list=media_list[0]
	for media in media_list:
		api.like_media(media.id)


