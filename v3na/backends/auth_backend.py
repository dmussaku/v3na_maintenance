from v3na.models import V3na_User

class AuthBackend(object):

    def authenticate(self, login=None, password=None):


        try:
            user = V3na_User.objects.get(login=login)
        except V3na_User.DoesNotExist:
            return None
        else:
            if password != None and user.password == password:
                return user
            return user

    def get_user(self, pk):
        r"""Returns user to use in templates"""
        try:
            return V3na_User.objects.get(pk=pk)
        except V3na_lUser.DoesNotExist:
            return None