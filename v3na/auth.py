from models import V3na_User
from django.http import HttpResponse, HttpResponseRedirect
# from auth.models import TestUser
import requests
from django.shortcuts import render


def user_login(request, *args, **kwargs):
	if request.method=='POST':
		login = request.POST.get('login', None)
		password = request.POST.get('password', None)
	else:
		login = request.GET.get('login', None)
		password = request.GET.get('password', None)		
	try:
		user = V3na_User.objects.get(login=login)
	except:
		user = None
		return render(request, 'response.html', {'response':"User name is incorrect"})
	if user:
		if user.password == password:
			return render(request, 'response.html', {'response':"OK"})
		else:
			return render(request, 'response.html', {'response':"Incorrect password"})
	else:
		r = requests.get('http://127.0.0.1:8000/auth_user/', auth=(login, password))
		print r

	# url = 'auth_user_view'
	return render(request, 'response.html', {'login':login, 'password':password})