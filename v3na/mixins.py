from models import V3na_User
from django.views.generic.detail import SingleObjectMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

class V3naUserObjectMixin(SingleObjectMixin):
    """
    Provides views with the current user's profile.
    """
    model = V3na_User

    def get_object(self):
        """Return's the current users profile."""
        try:
            return self.request.user
        except V3na_User.DoesNotExist:
            raise NotImplemented("No user")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """Ensures that only authenticated users can access the view."""
        klass = V3naUserObjectMixin
        return super(klass, self).dispatch(request, *args, **kwargs)