from django.db import models



class V3na_Server(models.Model):
	url_link=models.CharField(max_length=200)
	flag=models.IntegerField()

	def __unicode__(self):
		return self.url_link

class V3na_Host(models.Model):
	host=models.CharField(max_length=200)
	user=models.CharField(max_length=200)
	password=models.CharField(max_length=200)
	port=models.IntegerField()





